# encoding: utf-8

from flask_admin import Admin, AdminIndexView, expose
from flask_migrate import Migrate
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

from app.form.user import UserLoginForm


class IndexView(AdminIndexView):
    @expose('/')
    def index(self):
        form = UserLoginForm()
        return self.render('admin/index.html', form=form)


admin = Admin(index_view=IndexView())
api = Api()
db = SQLAlchemy()
migrate = Migrate()
