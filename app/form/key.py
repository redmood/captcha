# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired


class KeyCreateForm(FlaskForm):
    domains = StringField(
        _('Domains'),
        validators=[DataRequired()],
        description=_("Comma separated list of domains for this specific key,")
    )
