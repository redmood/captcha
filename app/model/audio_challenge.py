# encoding: utf-8

import os

import config
from app import admin, db
from app.model.model import Model, View


class AudioChallenge(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    # Token associated to the challenge itself
    token = db.Column(db.String(64))
    # Boolean flag indicating a "used" challenge to delete
    inactive = db.Column(db.Boolean, default=False)
    # Date and time
    date = db.Column(db.DateTime)
    # Solution of challenge
    solution = db.Column(db.String(100))
    # Author of source image
    author = db.Column(db.String(1000))
    # Licence of source image
    license = db.Column(db.String(1000))

    @property
    def filename(self):
        return "%s.ogg" % self.token

    @property
    def filepath(self):
        return os.path.join(
            config.AUDIO_CHALLENGE_DIR,
            self.filename[:2],
            self.filename[2:4],
            self.filename[4:6],
            self.filename,
        )

    @property
    def fileurl(self):
        if self.token.startswith(config.TEST_TOKEN):
            return os.path.join(
                "test/audio/challenges",
                self.filename[:2],
                self.filename[2:4],
                self.filename[4:6],
                self.filename,
            )
        return os.path.join(
            config.AUDIO_CHALLENGE_URL,
            self.filename[:2],
            self.filename[2:4],
            self.filename[4:6],
            self.filename,
        )


# Model will be automatically managed through flask-admin module
admin.add_view(View(AudioChallenge, db.session))
