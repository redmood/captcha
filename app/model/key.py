# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Key(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    # Public key to use for public scripts
    public = db.Column(db.String(40))
    # Private key to use for requesting solutions
    secret = db.Column(db.String(40))
    # Owner of the key
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref('keys', lazy='dynamic'))

    @property
    def public_secret(self):
        return self.secret[:2] + "..." + self.secret[-2:]


# Model will be automatically managed through flask-admin module
admin.add_view(View(Key, db.session))
