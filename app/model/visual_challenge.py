# encoding: utf-8

import os

import config
from app import admin, db
from app.model.model import Model, View


class VisualChallenge(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    # Token associated to the challenge itself
    token = db.Column(db.String(64))
    # Boolean flag indicating a "used" challenge to delete
    inactive = db.Column(db.Boolean, default=False)
    # Source's identifier for this particular challenge
    visual_source_id = db.Column(db.Integer, db.ForeignKey('visual_source.id'))
    visual_source = db.relationship(
        'VisualSource',
        backref=db.backref('challenges', lazy='dynamic')
    )

    @property
    def filename(self):
        return "%s.jpg" % self.token

    @property
    def filepath(self):
        return os.path.join(
            config.VISUAL_CHALLENGE_DIR,
            self.filename[:2],
            self.filename[2:4],
            self.filename[4:6],
            self.filename,
        )

    @property
    def fileurl(self):
        if self.token.startswith(config.TEST_TOKEN):
            return os.path.join(
                "test/visual/challenges",
                self.filename[:2],
                self.filename[2:4],
                self.filename[4:6],
                self.filename,
            )
        return os.path.join(
            config.VISUAL_CHALLENGE_URL,
            self.filename[:2],
            self.filename[2:4],
            self.filename[4:6],
            self.filename,
        )


# Model will be automatically managed through flask-admin module
admin.add_view(View(VisualChallenge, db.session))
